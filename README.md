Whether you prefer the classics or something with a TIPSY twist, this urban hip cocktail bar will definitely hit the spot. The terrace over-looking the views of Puerto Rico is the perfect setting to enjoy a drink from sun up to sun down while listening to chill house sounds and acoustic live music.

Address: The Market Puerto Rico local. Calle Tomas Roca Bosch 4, Local 3b, 35130 Puerto Rico, Las Palmas, Spain

Phone: +34 603 52 00 32

Website: http://www.tipsyelements.com
